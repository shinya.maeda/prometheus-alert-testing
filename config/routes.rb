Rails.application.routes.draw do
  get 'welcome/index'
  get 'welcome/generate_error'

  root 'welcome#index'
end
